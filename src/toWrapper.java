public class toWrapper {
    public static void autoBoxing() {
        byte bte = 10;
        short sh = 20;
        int it = 30;
        long lng = 4000;
        float fat = 50.0f;
        double dlb = 60.0D;
        char ch = 'c';
        boolean boo = false;
        /*
         * Autoboxing: converting primitives into objects
         * autoboxing là cơ chế tự động chuyển đổi kiểu dữ liệu nguyên thủy sang object của wrapper class tương ứng.
         */
        Byte byteobj = bte;
        Short shortobj = sh;
        Integer intobj = it;
        Long longobj = lng;
        Float floatobj = fat;
        Double doubleobj = dlb;
        Character chartobj = ch;
        Boolean boolobj = boo;

        System.out.println("---Printing object value (In giá trị của object)---");
        System.out.println("Byte object: " + byteobj);
        System.out.println("Short object: " + shortobj);
        System.out.println("Integer object: " + intobj);
        System.out.println("Long object: " + longobj);
        System.out.println("Float object: " + floatobj);
        System.out.println("Double object: " + doubleobj);
        System.out.println("Character object: " + chartobj);
        System.out.println("Boolean object: " + boolobj);
    }
    public static void main(String[] args) {
        toWrapper.autoBoxing();
    }

}
